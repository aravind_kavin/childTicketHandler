$(document).ready(function () {
    app.initialized()
        .then(function (_client) {
            var client = _client;
            client.events.on('app.activated',
                function () {
                    $('#conversationBox').empty();
                    client.data.get('ticket')
                        .then(function (ticketData) {
                            $('#apptext').hide();
                            var ticketId = ticketData.ticket.id;
                            var url = "https://spritle12.freshdesk.com/api/v2/tickets/" + ticketId;
                            var options = {
                                headers: {
                                    'content-type': 'application/json',
                                    'Authorization': 'Basic dFI1TmwzSEJ4UFk0a1pnMEducXo='
                                }
                            };
                            client.request.get(url, options).then(
                                function (data) {
                                    console.log(data);
                                    var viewticketData = JSON.parse(data.response);
                                    //console.log(viewticketData);
                                    var associationType = viewticketData.association_type;
                                    //console.log(associationType);
                                    if (associationType == 1 || associationType == null) {
                                        $('#complaintButton').show();
                                        var parentId = ticketId;
                                    }
                                    else if (associationType == 2) {
                                        $('#settlementButton').show();
                                        var parentId = viewticketData.associated_tickets_list[0];
                                    }
                                    $('.btn').click(function () {
                                        $('.btn').hide();
                                        var ticketSubject = ticketData.ticket.subject;
                                        var ticketDescription = ticketData.ticket.description_text;
                                        client.data.get('contact')
                                            .then(function (contactData) {
                                                var contactEmail = contactData.contact.email;
                                                var url = "https://spritle12.freshdesk.com/api/v2/tickets/" + ticketId + "/conversations";
                                                var options = {
                                                    headers: {
                                                        'content-type': 'application/json',
                                                        'Authorization': 'Basic dFI1TmwzSEJ4UFk0a1pnMEducXo='
                                                    }
                                                };
                                                client.request.get(url, options).then(
                                                    function (data) {
                                                        console.log(data);
                                                        var parsedConversationData = JSON.parse(data.response);
                                                        console.log(parsedConversationData);
                                                        var length = parsedConversationData.length;
                                                        var flag = 0;
                                                        if (length == 0)
                                                            $('#apptext').show();
                                                        else {
                                                            for (var i = 0; i < length; i++) {
                                                                console.log(parsedConversationData[i].body_text);
                                                                if (i >= length - 5) {
                                                                    var text = parsedConversationData[i].body_text;
                                                                    if (flag == 0)
                                                                        var conversation = "<input type='radio' name='radio' class='radio' id=" + i + " checked>  " + text.substring(0, 50) + "   <br><br>";
                                                                    else
                                                                        var conversation = "<input type='radio' name='radio' class='radio' id=" + i + ">  " + text.substring(0, 50) + "   <br><br>";
                                                                    $('#conversationBox').append(conversation);
                                                                    flag = 1;
                                                                }
                                                            }
                                                            var createticket = "<br><br><button type='button' class='btn'id='createTicket'>Create Ticket</button>"
                                                            $('#conversationBox').append(createticket);
                                                            $('#createTicket').click(function () {
                                                                var radioId = $('input[type=radio][name=radio]:checked').attr('id');
                                                                var childTicketTime = parsedConversationData[radioId].created_at;
                                                                var createticketurl = "https://spritle12.freshdesk.com/api/v2/tickets";
                                                                var options = {
                                                                    headers: {
                                                                        'content-type': 'application/json',
                                                                        'Authorization': 'Basic dFI1TmwzSEJ4UFk0a1pnMEducXo='
                                                                    },
                                                                    body: JSON.stringify({ "description": ticketDescription, "subject": ticketSubject, "email": contactEmail, "status": 2, "priority": 1, "source": 2, "parent_id": parentId })
                                                                };
                                                                var message = "Child Ticket has been created for ticket No:" + parentId;
                                                                client.request.post(createticketurl, options).then(
                                                                    function (success) {
                                                                        client.interface.trigger("showNotify", { type: "success", message: message });
                                                                    },
                                                                    function (error) {
                                                                        console.log(error);
                                                                    }
                                                                )

                                                            })
                                                        }

                                                    }
                                                )
                                            })

                                    });
                                });
                        });
                });
        });
});


